﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//These are Namespaces: locations of code that we need.

//This is a Class that inherits from MonoBehaviour
public class Trigger_Listener : MonoBehaviour
{
    //At the top of class, we declare our variables
    public bool playerEntered = false;

	// Use this for initialization
	void Start ()
    {
        Debug.Log("Start was Called");
	}
	
	// Update is called once per frame
	void Update ()
    {
        Debug.Log("Update was Called");
    }

    //Checks if player enters trigger
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            playerEntered = true;
        }
    }

    //Checks if player exits trigger
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            playerEntered = false;
        }
    }
}

/*Writing a forward slash and then star will allow you 
 to write notes/comments across
 multiple lines of text
 until you write a star and then forward slash
 like so...
 */