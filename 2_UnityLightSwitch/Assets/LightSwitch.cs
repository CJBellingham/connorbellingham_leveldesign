﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightSwitch : MonoBehaviour
{
    public Trigger_Listener trigger;
    public Image cursorImage;

    /*  Light Variable
        Audio Variable
        Animation Variable
    */

    public Light spotlight;

    public AudioSource Audio;

    Animation switchAnimation;

	// Use this for initialization
	void Start ()
    {
        Audio = GetComponent<AudioSource>();
        switchAnimation = GetComponent<Animation>();
        cursorImage.enabled = false;
	}
	
	// Called when cursor is over object
	void OnMouseOver ()
    {
        if(trigger.playerEntered == true)
        {
            if(cursorImage.enabled != true)
            {
                cursorImage.enabled = true;
            }
            Debug.Log("Mouse Over Switch");
        }
        else
        {
            cursorImage.enabled = false;
        }
	}

    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
        }
    }

    void OnMouseDown()
    {
        if (trigger.playerEntered == true)
        {
            Audio.Play();
            switchAnimation.Stop();
            switchAnimation.Play();

            if(spotlight.intensity > 0f)
            {
                spotlight.intensity = 0f;
            }
            else
            {
                spotlight.intensity = 2f;
            }
        }
    }
}