﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Door_Rotate : MonoBehaviour

{
    public GameObject door;
    GameObject myPlayer;
    public float targetRot = 90f;
    bool doorOpen;
    Vector3 closedRot;
    public float speed = 5f;
    public Mouse_Listen mouse;
    public Image cursorImage;
    bool inTrigger;
    bool clickHappened;


    void Start ()
    {
        myPlayer = GameObject.FindGameObjectWithTag("Player");
        closedRot = door.transform.localRotation.eulerAngles;
	}

    void Update()
    {
        if(inTrigger == true)
        {
            if(mouse.mouseCursorOn == true)
            {
                if(cursorImage.enabled == false)
                {
                    cursorImage.enabled = true;
                }
            }
            else
            {
                cursorImage.enabled = false;
            }
        }
        if(inTrigger == true && mouse.mouseClicked == true && clickHappened == false)
        {
            clickHappened = true;
            DoorInteract();
        }
        else if(mouse.mouseClicked == false)
        {
            clickHappened = false;
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            inTrigger = true;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            inTrigger = false;
            cursorImage.enabled = false;
        }
    }
    void DoorInteract()
    {
        Vector3 finishRot;
        if(doorOpen == false)
        {
            Vector3 playerDir = door.transform.position - myPlayer.transform.position;
            float dot = Vector3.Dot(playerDir, transform.forward);
            doorOpen = true;

            if (dot > 0f)
            {
                finishRot = new Vector3(closedRot.x, closedRot.y + targetRot, closedRot.z);
            }
            else
            {
                finishRot = new Vector3(closedRot.x, closedRot.y - targetRot, closedRot.z);
            }
        }
        else
        {
           finishRot =closedRot;
            doorOpen = false;
        }
        StopCoroutine("DoorMotion");
        StartCoroutine("DoorMotion", finishRot);
    }

    IEnumerator DoorMotion(Vector3 target)
    {
        while(Quaternion.Angle(door.transform.localRotation, Quaternion.Euler(target)) > 0.02f)
        {
            door.transform.localRotation = Quaternion.Slerp(door.transform.localRotation, Quaternion.Euler(target), speed * Time.deltaTime);
            yield return null;
        }
        door.transform.localRotation = Quaternion.Euler(target);
        yield return null;
    }

}
