﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouse_Listen : MonoBehaviour
{

    public bool mouseCursorOn;
    public bool mouseClicked;

    void OnMouseOver()
    {
       if(mouseCursorOn == false)
        {
            mouseCursorOn = true;
        } 
    }
    void OnMouseExit()
    {
        mouseCursorOn = true;
    }
    void OnMouseDown()
    {
        mouseClicked = true;
    }
    void OnMouseUp()
    {
        mouseClicked = false;
    }

}